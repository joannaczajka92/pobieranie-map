<!DOCTYPE html>     
<html>      
   <head>      
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />      
		<meta name="viewport" content="initial-scale=1.0, width=device-width" />

		<title>Pobieranie map</title>      

		<script src="lib/openlayers-master/openlayers-master/lib/OpenLayers.js"></script>
		<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAxSE1XYBYkBp6_P3mn_s37ga59ZMcUbJE&callback=initMap"type="text/javascript"></script>
		<!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>-->
		<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
		<script type="text/javascript" src="http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0"></script>
		<script type="text/javascript" src="http://js.api.here.com/se/2.5.3/jsl.js?with=all" charset="utf-8"></script>
		<script src="http://js.api.here.com/v3/3.0/mapsjs-core.js" type="text/javascript" charset="utf-8"></script>
		<script src="http://js.api.here.com/v3/3.0/mapsjs-service.js" type="text/javascript" charset="utf-8"></script>
		<script src="http://js.api.here.com/v3/3.0/mapsjs-ui.js" type="text/javascript" charset="utf-8"></script>
		<script src="http://cdn.leafletjs.com/leaflet-0.5.1/leaflet.js"></script>
		<script src="http://leaflet.github.com/Leaflet.label/leaflet.label.js"></script>
		<script src="http://olegsmith.github.com/leaflet.wikimapia/leaflet.wikimapia.js"></script>
		<link rel="stylesheet" type="text/css" href="http://js.api.here.com/v3/3.0/mapsjs-ui.css">
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<script src="http://js.api.here.com/v3/3.0/mapsjs-mapevents.js" type="text/javascript" charset="utf-8"></script>
		<script type="text/javascript" src="js/openStreetMap.js"></script>
		<script type="text/javascript" src="js/googleMaps.js"></script>
		<script type="text/javascript" src="js/bingMaps.js"></script>
		<script type="text/javascript" src="js/hereMap.js"></script>
		<script type="text/javascript" src="js/wikimapia.js"></script>
		<script type="text/javascript" src="js/loadMap.js"></script>
		<script type="text/javascript" src="js/getMap.js"></script>

   </head>    
	
   <body onload="mapStart()" >
       
		<div id="top">
			<div id="head"></div>
				<div id="main" class="main-box">
					<form id="mapForm" name="Form1" method="get">
							<div class="first-half">Mapa
								<select name="mapaSel" id="mapChoose" style="width:200px">
									<option value="GoogleMaps">Google Maps</option>
									<option value="OpenStreetMap">OpenStreetMap</option>
									<option value="BingMaps">Bing Maps</option>
									<option value="HereMap">Here Map</option>
									<option value="Wikimapia">Wikimapia</option>
								</select>
							</div>
			
							<div class="snd-half">Typ<br>
								<select name="mapaTypeGoogle" id="googleType" style="width:200px">
								<option value="roadmap">Roadmap</option>
								<option value="satelliteGoogle">Satellite</option>
								<option value="hybrid">Hybrid</option>
								<option value="terrain">Terrain</option>
								</select>
								
								<select name="mapaTypeOSM" id="OpenStreetMapType" style="width:200px">
								<option value="basic">Basic</option>
								<option value="cycleMap">CycleMap</option>
								</select>
						
								<select name="mapaBingType" id="BingType" style="width:200px">
								<option value="basicBing">Basic</option>
								<option value="satellite">Satellite</option>
								<option value="satelliteLabel">Satellite+Label</option>
								</select>
								
								<select name="mapaTypeHere" id="HereType" style="width:200px">
								<option value="base">Base</option>
								<option value="satHere">Satellite</option>
								<option value="hybHere">Hybrid</option>
								<option value="terHere">Terrain</option>
								<option value="traHere">Traffic</option>
								</select>
								
								<select name="mapaWiki" id="WikiType" style="width:200px">
								<option value="wikiBase"></option>
								
								</select>
						
							</div><div style="clear:both"></div>
							<br>
							Powiększenie mapy
							<br>
							<input id="scaleMap" type="" name="zoomMap"  value="10" style="width:50px" readonly="readonly"  />
												
							<p id="info">Współrzędne geograficzne</p>
						 
							<div class="first-half">Szerokość <input onkeyup="setPositionMap()" type="text" name="lat" id="lat_id" value="50.0267188" style="width:180px" /></div>
							<div class="snd-half">Długość <input onkeyup="setPositionMap()" type="text" name="lng" id="lng_id" value="22.0186674" style="width:180px" /></div>
						   
							<div style="clear:both"></div><br><br>
							
							<div class="first-half1">Adres<br>
							Powiększenie<br>
							Nazwa pliku<br>
							Przewidywany rozmiar</div>
							<div class="snd-half1"><input id="address" type="textbox" value=""> <input type="button" class="btn" value="Szukaj" ><br><br>
												 
								<input id="getSizeImage" class="slider" type="range" name="amountRange" min="1" max="" value="12" oninput="this.form.amountInput.value=this.value"/>
								<input id="scale" type="text" name="amountInput" min="1" max="" value="12" size="50" style="width: 50px" readonly="readonly" /> <br><br>
								<input id="fileText" type="text" name="nameFile" value="mapa_obraz" /><input type="radio" class="check-input" name="format" value="jpg">.jpg <input type="radio" class="check-input" name="format" checked="checked" value="png">.png<br><br>
								<input id="sizeImage" type="text" value="" readonly="readonly"> MB
									
							</div>
							<div style="clear:both"></div><br>
							<div id="processResult"></div>
							
							<input id="downloadMap" class="btn" type="submit" value="Pobierz obszar" name="button1">

					</form>
				
			</div>
			<div id="mapBox" class="main-box">
					
				<div id="basicMap"></div>

			</div> 
		</div>
			<div id="footer"></div>
			</div>
		
		
	</body>      
</html> 