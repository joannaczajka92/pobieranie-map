<?php

set_time_limit(0);
ini_set("memory_limit","3800M");

$lon=(float)$_GET['lng'];
$lat=(float)$_GET['lat'];

$zoom=(int)$_GET['amountInput'];
$zoomMap=(int)$_GET['zoomMap'];
$xs = floor((($lon + 180) / 360) * pow(2, $zoom));
$ys = floor((1 - log(tan(deg2rad($lat)) + 1 / cos(deg2rad($lat))) / pi()) /2 * pow(2, $zoom));

$widthMap=pow(2, ($zoom-$zoomMap))*2;
$heightMap=pow(2, ($zoom-$zoomMap))*2;

$mapa=imagecreate($widthMap*256, $heightMap*256);

$xs-=$widthMap/2;
$ys-=$heightMap/2;

for($x=0; $x<=$widthMap; $x++) {

	for($y=0; $y<=$heightMap; $y++) {
	
		$num=($xs+$x)%4+((($ys+$y)%4)*4);
		$url="http://i".((string)($num)).".wikimapia.org/?x=".((string)($xs+$x))."&y=".((string)($ys+$y))."&zoom=".((string)($zoom))."&r=1&type=&lng=0";
		$src = imagecreatefrompng($url);		
		imagecopymerge($mapa, $src, ($x*256), ($y*256), 0, 0, 256, 256, 100);
		imagedestroy($src);
		
	}
}

if ($_GET['format'] == 'png') {
	
	imagepng($mapa, ''.$_GET['nameFile'].'.'.$_GET['format'].'');
	
} else {
	
	imagejpeg($mapa, ''.$_GET['nameFile'].'.'.$_GET['format'].'');
}

imagedestroy($mapa);
	
header('Content-Type: image/png');

?>
		
		
