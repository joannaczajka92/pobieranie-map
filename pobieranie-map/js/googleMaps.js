function mapStart()   
{   
  scaleSize(21,21);
  var lat = document.getElementById("lat_id").value;
  var lon = document.getElementById("lng_id").value;
  var zoom = document.getElementById("scaleMap").value;
  document.getElementById("OpenStreetMapType").style.display='none';
  document.getElementById("BingType").style.display='none';
  document.getElementById("HereType").style.display='none';	
  document.getElementById("WikiType").style.display='none';
  
				  
  var mapType = document.getElementById('googleType').value;
  if(mapType == 'roadmap'){
	var type='roadmap';
	
	 }else if(mapType == 'satelliteGoogle'){
		scaleSize(19,19);
		var type='satellite';
	 }else if(mapType == 'hybrid'){
		scaleSize(19,19);
		var type='hybrid';
	 }else if(mapType == 'terrain'){
		scaleSize(15,15);
		var type='terrain';
 }
  
	geocoder = new google.maps.Geocoder();
	var lngLat = new google.maps.LatLng(parseFloat(lat), parseFloat(lon));
	var optionMap = {
	  zoom: parseInt(zoom),
	  center: lngLat,
	  disableDefaultUI: false,
	  navigationControl: true,
	  mapTypeId: type
	};
	mapObj = new google.maps.Map(document.getElementById("basicMap"), optionMap); 
	
	initGoogleListener();
}  


function initGoogleListener() {
	
	googleListener = google.maps.event.addListener(mapObj,'idle',function()
	  {
	   document.activeElement.blur();
		// wyznaczamy widoczny obszar
		var centerPoint = mapObj.getCenter();
	
		 // pomocnicze współrzędne
		var lng_center   = centerPoint.lng();
		var lat_center   = centerPoint.lat();			   
		 
		document.getElementById("lat_id").value=lat_center;
		document.getElementById("lng_id").value=lng_center;
		
		var zoomMap = mapObj.getZoom();
		if(!zoomMap)
			return;
		 document.getElementById("scaleMap").value=zoomMap;
		 getSizeImage();	
	  });
}