//Bing Maps

function getMap(){
  
	var zoom = document.getElementById("scaleMap").value;
	var lat = document.getElementById("lat_id").value;
	var lon = document.getElementById("lng_id").value;
	scaleSize(20,20);
	var mapType = document.getElementById('BingType').value;

	if(mapType == 'basicBing'){
		
		var typeBing = Microsoft.Maps.MapTypeId.road;
		
	}else if(mapType == 'satellite'){
		
		var typeBing = Microsoft.Maps.MapTypeId.aerial;
		var labelBing = Microsoft.Maps.LabelOverlay.hidden;
		
	}else if(mapType == 'satelliteLabel'){
		
		var typeBing = Microsoft.Maps.MapTypeId.aerial;
		var labelBing = Microsoft.Maps.LabelOverlay.visable;
	}
		
	document.getElementById("googleType").style.display='none';	
	document.getElementById("OpenStreetMapType").style.display='none';	
	document.getElementById("HereType").style.display='none';	
	document.getElementById("WikiType").style.display='none';

	mapBing = new Microsoft.Maps.Map(document.getElementById('basicMap'), {credentials: 'Aje83y0cjkkPrxkaY45x0CoTRkLWcZSq-RFqqyW47al14l1RNTGV8v6SGmyLuKED', 
	center: new Microsoft.Maps.Location(parseFloat(lat), parseFloat(lon)), 
	zoom: parseInt(zoom), 
	mapTypeId: typeBing,
	labelOverlay: labelBing,
	showMapTypeSelector: true, 
	showScalebar: false,
	showDashboard: false});
	  
	Microsoft.Maps.Events.addHandler(mapBing, 'copyrightchanged', function(){
		
		var centerPoint = mapBing.getCenter();
		var lng_center   = centerPoint.longitude;
		var lat_center   = centerPoint.latitude;
		
		document.getElementById("lat_id").value=lat_center;
		document.getElementById("lng_id").value=lng_center;
		  
		var zoomMap = mapBing.getZoom();
		document.getElementById("scaleMap").value=zoomMap;
		getSizeImage();	
		
	});
	  
  
} 