
function init() {

  var lat = document.getElementById("lat_id").value;
  var lon = document.getElementById("lng_id").value;
  var zoom = document.getElementById("scaleMap").value;

  document.getElementById("googleType").style.display='none';
  document.getElementById("BingType").style.display='none';
  document.getElementById("HereType").style.display='none';	
  document.getElementById("WikiType").style.display='none';
  scaleSize(18,18);
  mapOpenStreetMap = new OpenLayers.Map("basicMap", {
	eventListeners: {
					"moveend": mapEvent,
				  
					 }
  });
  
	var mapnik         = new OpenLayers.Layer.OSM();
	var cycle          = new OpenLayers.Layer.OSM("OpenCycleMap", ['http://tile.thunderforest.com/cycle/${z}/${x}/${y}.png']);
	var fromProjection = new OpenLayers.Projection("EPSG:4326");   // Transform from WGS 1984
	var toProjection   = new OpenLayers.Projection("EPSG:900913"); // to Spherical Mercator Projection
	var position       = new OpenLayers.LonLat(parseFloat(lon), parseFloat(lat)).transform( fromProjection, toProjection);
	var zoom           = parseInt(zoom); 
	
	var mapType = document.getElementById('OpenStreetMapType').value;
	
	if(mapType == 'basic'){
		
		scaleSize(18,18);
		mapOpenStreetMap.addLayer(mapnik);
		
	 }else if(mapType == 'cycleMap'){
		
		mapOpenStreetMap.addLayer(cycle);
	 }
	
	mapOpenStreetMap.setCenter(position, zoom );

	function mapEvent(event) {
				console.log(event.type);
				
				
				var zoomMap = mapOpenStreetMap.getZoom();
				document.getElementById("scaleMap").value=zoomMap;
				getSizeImage();				
								
				var centerPoint = mapOpenStreetMap.getCenter().transform(mapOpenStreetMap.getProjectionObject(),
								   new OpenLayers.Projection("EPSG:4326"));
				
				var lng_center   = centerPoint.lon;
				var lat_center   = centerPoint.lat;			   
				
				document.getElementById("lat_id").value=lat_center;
				document.getElementById("lng_id").value=lng_center;
			}		       
								
}

function scaleSize(slider,scale){

	$(function(){
		$("input[name='amountInput']").prop('max',parseInt(slider));
		$("input[name='amountRange']").prop('max',parseInt(scale));
	});
}

