
function hereMap(){
	  
	var lat = document.getElementById("lat_id").value;
	var lon = document.getElementById("lng_id").value;
	var zoom = document.getElementById("scaleMap").value;
	document.getElementById("OpenStreetMapType").style.display='none';
	document.getElementById("BingType").style.display='none';
	document.getElementById("googleType").style.display='none';	
	document.getElementById("WikiType").style.display='none';
	scaleSize(20,20);

	var platform = new H.service.Platform({
		'app_id': 'QzwLrGsKAQCxla3EP7hz',
		'app_code': 'UUbizE095WZ8Jpc3pEjHWQ'
		});
		
	var mapType = document.getElementById('HereType').value;
		  
	var defaultLayers = platform.createDefaultLayers();

	if(mapType=="base"){

	mapHere = new H.Map(
		  document.getElementById('basicMap'),
		  defaultLayers.normal.map,
		  {
			zoom: parseInt(zoom),
			scalebar: true,
			center: {lat: parseFloat(lat), lng: parseFloat(lon)}
			
		  });
		  
	}else if(mapType=="satHere"){

	mapHere = new H.Map(
		  document.getElementById('basicMap'),
		  defaultLayers.satellite.xbase,
		  {
			zoom: parseInt(zoom),
			scalebar: true,
			center: {lat: parseFloat(lat), lng: parseFloat(lon)}
			
		  });
		  
	}else if(mapType=="hybHere"){	

	mapHere = new H.Map(
		  document.getElementById('basicMap'),
		  defaultLayers.satellite.map,
		  {
			zoom: parseInt(zoom),
			scalebar: true,
			center: {lat: parseFloat(lat), lng: parseFloat(lon)}
			
		  });
	}else if(mapType=="terHere"){
		
	mapHere = new H.Map(
		  document.getElementById('basicMap'),
		  defaultLayers.terrain.map,
		  {
			zoom: parseInt(zoom),
			scalebar: true,
			center: {lat: parseFloat(lat), lng: parseFloat(lon)}
			
		  });
		  
	}else if(mapType=="traHere"){
		
	mapHere = new H.Map(
		  document.getElementById('basicMap'),
		  defaultLayers.normal.traffic,
		  {
			zoom: parseInt(zoom),
			scalebar: true,
			center: {lat: parseFloat(lat), lng: parseFloat(lon)}
			
		  });
	}
		  
	var ui = H.ui.UI.createDefault(mapHere, defaultLayers, 'pl-PL');

	var mapEvents = new H.mapevents.MapEvents(mapHere);
	mapHere.addEventListener('mapviewchangeend', function() {
		 
		var zoomMap = mapHere.getZoom();
		document.getElementById("scaleMap").value=zoomMap;
		
		var centerPoint = mapHere.getCenter();
		var lng_center   = centerPoint.lng;
		var lat_center   = centerPoint.lat;	
		document.getElementById("lat_id").value=lat_center;
		document.getElementById("lng_id").value=lng_center;
		var zoomMap = mapHere.getZoom();
		document.getElementById("scaleMap").value=zoomMap;
		getSizeImage();	
		
	}); 
									 
	var behavior = new H.mapevents.Behavior(mapEvents);
			
	var mapSettings = ui.getControl('mapsettings');
	var zoom = ui.getControl('zoom');
	var scalebar = ui.getControl('scalebar');
	var panorama = ui.getControl('panorama');
				  
}