
document.getElementById('#mapChoose').onchange = function() {loadMap()};
document.getElementById('#googleType').onchange = function() {loadMap()};
document.getElementById('#OpenStreetMapType').onchange = function() {loadMap()};
document.getElementById('#BingType').onchange = function() {loadMap()};
document.getElementById('#HereType').onchange = function() {loadMap()};
document.getElementById('#WikiType').onchange = function() {loadMap()};

function loadMap(){
  
document.getElementById("basicMap").innerHTML=null;

var mylist = document.getElementById("mapChoose").value;
  if(mylist == "OpenStreetMap"){
	 init();
	 document.getElementById("OpenStreetMapType").style.display='';
	 
  }else if(mylist == "GoogleMaps"){
	 document.getElementById("googleType").style.display='';
	 mapStart();
	 
  }else if(mylist == "BingMaps"){
	document.getElementById("OpenStreetMapType").style.display='none';
	document.getElementById("BingType").style.display='';
	getMap();
	
  }else if(mylist == "HereMap"){
	document.getElementById("HereType").style.display='';
	hereMap();
	
  }else if(mylist == "Wikimapia"){
	document.getElementById("WikiType").style.display='';
	wikiMapia();
	  
  }
}

//Ustawianie pozycji
function setPositionMap(){

var setLat = document.getElementById("lat_id").value;
var setLng = document.getElementById("lng_id").value;
var mylist = document.getElementById("mapChoose").value;

if(mylist == "GoogleMaps"){
	
	var myLatLng = new google.maps.LatLng(parseFloat(setLat), parseFloat(setLng));
	google.maps.event.removeListener(googleListener);
	mapObj.setCenter(myLatLng);
	initGoogleListener();
	
}else if(mylist == "OpenStreetMap"){
 
  var fromProjection = new OpenLayers.Projection("EPSG:4326");   // Transform from WGS 1984
  var toProjection   = new OpenLayers.Projection("EPSG:900913"); // to Spherical Mercator Projection
 
  var position = new OpenLayers.LonLat(parseFloat(setLng), parseFloat(setLat)).transform( fromProjection, toProjection);
  mapOpenStreetMap.setCenter(position);

}else if(mylist == "BingMaps"){

	var positionBing = new Microsoft.Maps.Location(parseFloat(setLat),parseFloat(setLng));
	mapBing.setView({center:positionBing});

}else if(mylist == "HereMap"){

	var geoPoint = new H.geo.Point(parseFloat(setLat), parseFloat(setLng));
	mapHere.setCenter(geoPoint);

}else if(mylist == "Wikimapia"){
	
	document.getElementById("basicMap").innerHTML=null;
	wikiMapia();
	
}
}   

document.getElementById('#address').onclick = function() {codeAddress()};

//Pobieranie i ustawianie adresu
function codeAddress() {
	
	var address = document.getElementById('address').value;
	geocoder.geocode( { 'address': address}, function(results, status) {

	var addressLat =results[0].geometry.location.lat();
	var addressLot =results[0].geometry.location.lng();

	document.getElementById("lat_id").value=addressLat;
	document.getElementById("lng_id").value=addressLot;
	setPositionMap();

});
}