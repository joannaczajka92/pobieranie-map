
function processMapRequest(targetUrl) {			

	var resultInfo = document.getElementById("processResult");
	resultInfo.innerHTML = "Zapisywanie pliku...";
	document.body.style.cursor = "progress";
	$('#mapForm').unbind('submit').bind('submit', function (e) {		
		console.log("Form1 submit "+mylist);
		e.preventDefault();
		e.stopPropagation();
	  $.ajax({
		type: 'get',
		url: targetUrl,
		data: $('#mapForm').serialize(),
		success: function () {
		console.log("Succes "+mylist);
		resultInfo.innerHTML = "Obraz został zapisany pomyślnie";
		document.body.style.cursor = "auto";
		 },
		cache: false
	  });

	});

}

document.getElementById('#downloadMap').onclick = function() {downloadMap()};


function downloadMap()
{
	var mapScale = document.getElementById("scaleMap").value;
	var outputImgScale = document.getElementById("scale").value;
	
	if(parseInt(mapScale) > parseInt(outputImgScale)) {
		var resultInfo = document.getElementById("processResult");
		resultInfo.innerHTML = "Powiększenie mapy nie może być większe od powiększenia obrazu!";
		return;
	}
	mylist = document.getElementById("mapChoose").value;

	if(mylist == "OpenStreetMap"){
		
		processMapRequest("openstreetmap.php");
  
	}else if(mylist == "GoogleMaps"){
		
		processMapRequest("googlemaps.php");
		
	}else if(mylist == "BingMaps"){
		
		processMapRequest("bings.php");
		
	}else if(mylist == "HereMap"){

		processMapRequest("hereBase.php")
		
	}else if(mylist == "Wikimapia"){
		
		processMapRequest("wikimapia.php")
	
	}
}


document.getElementById('#getSizeImage').onclick = function() {getSizeImage()};
//Pobieranie rozmiaru obrazu
function getSizeImage(){
	
	var resultInfo = document.getElementById("processResult");
	var scaleMaps = document.getElementById("scaleMap").value;
	var scaleInput = document.getElementById("scale").value;
	
	document.getElementById("getArea").disabled = false;	
	resultInfo.innerHTML = "";
	if(parseInt(scaleMaps) > parseInt(scaleInput)) {	
	
		var message = "Powiększenie mapy nie może być większe od powiększenia obrazu!";
		var colorMessage = message.fontcolor("red");
		resultInfo.innerHTML = colorMessage;
		document.getElementById("getArea").disabled = true;		
	}
	if((parseInt(scaleInput) - parseInt(scaleMaps)) > 5) {
		
		var message = "Obraz jest zbyt duży!";
		var colorMessage = message.fontcolor("red");
		resultInfo.innerHTML = colorMessage;
		document.getElementById("getArea").disabled = true;
	}
	
	var googleType = document.getElementById("googleType").value;
	var osmType = document.getElementById("OpenStreetMapType").value;
	var bingType = document.getElementById("BingType").value;
	var hereType = document.getElementById("HereType").value;
	var wikiType = document.getElementById("mapChoose").value;
	var size = Math.pow(2,parseInt(scaleInput)-parseInt(scaleMaps))*2;
	if(googleType == "satelliteGoogle" || googleType =="hybrid" || bingType == "satellite" || bingType == "satelliteLabel" || hereType == "satHere" || hereType == "hybHere" ){
	
		var sizeArea = (size*size*125)/1024; 
	
	}else{
		
		var sizeArea = (size*size*60)/1024; 
		
	}

	document.getElementById("sizeImage").value=sizeArea;
	
}
