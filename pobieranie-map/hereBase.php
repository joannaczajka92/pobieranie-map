<?php
 
set_time_limit(0);
ini_set("memory_limit","3800M");

$lon=(float)$_GET['lng'];
$lat=(float)$_GET['lat'];
$zoom=(int)$_GET['amountInput'];
$zoomMap=(int)$_GET['zoomMap'];
$xs = floor((($lon + 180) / 360) * pow(2, $zoom));
$ys = floor((1 - log(tan(deg2rad($lat)) + 1 / cos(deg2rad($lat))) / pi()) /2 * pow(2, $zoom));

			
$widthMap=pow(2, ($zoom-$zoomMap))*2;
$heightMap=pow(2, ($zoom-$zoomMap))*2;

$mapa=imagecreatetruecolor($widthMap*256, $heightMap*256);

$xs-=$widthMap/2;
$ys-=$heightMap/2;

if($_GET['mapaTypeHere'] == 'base') {

	$url_s="http://2.base.maps.cit.api.here.com/maptile/2.1/maptile/newest/normal.day/";
	for($x=0; $x<=$widthMap; $x++) {

		for($y=0; $y<=$heightMap; $y++) {
					 
			$url=$url_s.((string)($zoom))."/".((string)($xs+$x))."/".((string)($ys+$y))."/256/png8?app_id=DemoAppId01082013GAL&app_code=AJKnXv84fjrb0KIHawS0Tg";	
			$src = imagecreatefrompng($url);
			imagecopymerge($mapa, $src, ($x*256), ($y*256), 0, 0, 256, 256, 100);
			imagedestroy($src);
			
		}
	}
		
}else if($_GET['mapaTypeHere'] == 'satHere' ) {

	for($x=0; $x<=$widthMap; $x++) {

		for($y=0; $y<=$heightMap; $y++) {
			
			$num=1+((($xs+$x)+($ys+$y))%4);
			$url="http://".((string)($num)).".aerial.maps.cit.api.here.com/maptile/2.1/maptile/newest/satellite.day/".((string)($zoom))."/".((string)($xs+$x))."/".((string)($ys+$y))."/256/png8?app_id=DemoAppId01082013GAL&app_code=AJKnXv84fjrb0KIHawS0Tg";
			$src = imagecreatefrompng($url);							
			imagecopymerge($mapa, $src, ($x*256), ($y*256), 0, 0, 256, 256, 100);
			imagedestroy($src);
	}
}

			
}else if($_GET['mapaTypeHere'] == 'hybHere' ) {

	for($x=0; $x<=$widthMap; $x++) {

		for($y=0; $y<=$heightMap; $y++) {
		
			$num=1+((($xs+$x)+($ys+$y))%4);
			$url="http://".((string)($num)).".aerial.maps.cit.api.here.com/maptile/2.1/maptile/newest/hybrid.day/".((string)($zoom))."/".((string)($xs+$x))."/".((string)($ys+$y))."/256/png8?app_id=DemoAppId01082013GAL&app_code=AJKnXv84fjrb0KIHawS0Tg";
			$src = imagecreatefrompng($url);				
			imagecopymerge($mapa, $src, ($x*256), ($y*256), 0, 0, 256, 256, 100);			
			imagedestroy($src);
		
		}

	}
		
}else if($_GET['mapaTypeHere'] == 'terHere' ) {

	for($x=0; $x<=$widthMap; $x++) {

		for($y=0; $y<=$heightMap; $y++) {
			
			$num=1+((($xs+$x)+($ys+$y))%4);
			$url="http://".((string)($num)).".aerial.maps.cit.api.here.com/maptile/2.1/maptile/newest/terrain.day/".((string)($zoom))."/".((string)($xs+$x))."/".((string)($ys+$y))."/256/png8?app_id=DemoAppId01082013GAL&app_code=AJKnXv84fjrb0KIHawS0Tg";
			$src = imagecreatefrompng($url);						
			imagecopymerge($mapa, $src, ($x*256), ($y*256), 0, 0, 256, 256, 100);
			imagedestroy($src);

		}

	}
		
}else if($_GET['mapaTypeHere'] == 'traHere' ) {
	
	for($x=0; $x<=$widthMap; $x++) {
	
		for($y=0; $y<=$heightMap; $y++) {
			
			$num=1+((($xs+$x)+($ys+$y))%4);
			$url="http://".((string)($num)).".traffic.maps.cit.api.here.com/maptile/2.1/traffictile/newest/normal.day/".((string)($zoom))."/".((string)($xs+$x))."/".((string)($ys+$y))."/256/png8?app_id=DemoAppId01082013GAL&app_code=AJKnXv84fjrb0KIHawS0Tg";
			$src = imagecreatefrompng($url);				
			imagecopymerge($mapa, $src, ($x*256), ($y*256), 0, 0, 256, 256, 100);
			imagedestroy($src);
		}
	
	}
}
		

if($_GET['format'] == 'png') {

imagepng($mapa, ''.$_GET['nameFile'].'.'.$_GET['format'].'');

}else {
	
imagejpeg($mapa, ''.$_GET['nameFile'].'.'.$_GET['format'].'');

}

imagedestroy($mapa);

header('Content-Type: image/jpeg');


?>
		
		
