<?php

set_time_limit(0);
ini_set("memory_limit","3800M");

function get_content($url) { 

   $ch = curl_init();  
   curl_setopt ($ch, CURLOPT_URL, $url);  
   curl_setopt ($ch, CURLOPT_HEADER, 0);  
   curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.9) Gecko/20071025 Firefox/2.0.0.9');
  
   ob_start();  
  
   curl_exec ($ch);  
   curl_close ($ch);  
   $string = ob_get_contents();  
  
   ob_end_clean();  
   return $string;      
  
}  

ini_set('user_agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.9) Gecko/20071025 Firefox/2.0.0.9');

function get_data($url) {

	$ch = curl_init();
	$timeout = 5;
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
	curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
	$data = curl_exec($ch);
	curl_close($ch);
	return $data;
	
}

$lon=(float)$_GET['lng'];
$lat=(float)$_GET['lat'];

$zoom=(int)$_GET['amountInput'];
$xs = floor((($lon + 180) / 360) * pow(2, $zoom));
$ys = floor((1 - log(tan(deg2rad($lat)) + 1 / cos(deg2rad($lat))) / pi()) /2 * pow(2, $zoom));

$zoomMap=(int)$_GET['zoomMap'];

$widthMap=pow(2, ($zoom-$zoomMap))*2;
$heightMap=pow(2, ($zoom-$zoomMap))*2;

$mapa=imagecreatetruecolor($widthMap*256, $heightMap*256);

$xs-=$widthMap/2;
$ys-=$heightMap/2;

if($_GET['mapaTypeGoogle'] == 'roadmap') {
	
	$url_s="https://mts0.google.com/vt/lyrs=m@248000000&hl=pl&src=app&x=";
	
	for($x=0; $x<$widthMap; $x++) {
	
		for($y=0; $y<$heightMap; $y++) {
		
		$url=$url_s.((string)($xs+$x))."&y=".((string)($ys+$y))."&z=".((string)($zoom))."&s=";			
		$src = imagecreatefrompng($url);							
		imagecopymerge($mapa, $src, ($x*256), ($y*256), 0, 0, 256, 256, 100);		
		imagedestroy($src);
		}
	}
			
}else if($_GET['mapaTypeGoogle'] == 'satelliteGoogle') {
	
	$url_s="http://mt1.google.com/vt/lyrs=s&x=";
	
	for($x=0; $x<$widthMap; $x++) {
	
		for($y=0; $y<$heightMap; $y++) {
			
			$url=$url_s.((string)($xs+$x))."&y=".((string)($ys+$y))."&z=".((string)($zoom))."&s=";		
			$src = imagecreatefromjpeg($url);							
			imagecopymerge($mapa, $src, ($x*256), ($y*256), 0, 0, 256, 256, 100);	
			imagedestroy($src);
			
		}
	}
		
}else if($_GET['mapaTypeGoogle'] == 'hybrid') {
	
	$url_s="http://mt1.google.com/vt/lyrs=y&x=";
	
	for($x=0; $x<$widthMap; $x++) {
	
		for($y=0; $y<$heightMap; $y++) {
		
			$url=$url_s.((string)($xs+$x))."&y=".((string)($ys+$y))."&z=".((string)($zoom))."&s=";		
			$src = imagecreatefromjpeg($url);						
			imagecopymerge($mapa, $src, ($x*256), ($y*256), 0, 0, 256, 256, 100);
			imagedestroy($src);
		}
	}

}else if($_GET['mapaTypeGoogle'] == 'terrain'){
	
	$url_s="http://mts0.google.com/vt/lyrs=t@130,r@212000000&hl=en&src=app&x=";

	for($x=0; $x<$widthMap; $x++) {

		for($y=0; $y<$heightMap; $y++) {
	
			$url=$url_s.((string)($xs+$x))."&y=".((string)($ys+$y))."&z=".((string)($zoom))."&s=";
			$src = imagecreatefromjpeg($url);					
			imagecopymerge($mapa, $src, ($x*256), ($y*256), 0, 0, 256, 256, 100);
			imagedestroy($src);
		
		}	
	}
}
			
	if($_GET['format'] == 'png') {
	
		imagepng($mapa, ''.$_GET['nameFile'].'.'.$_GET['format'].'');
	
	}else {
		
		imagejpeg($mapa, ''.$_GET['nameFile'].'.'.$_GET['format'].'');
	}
	
	imagedestroy($mapa);

	
header('Content-Type: image/png');


?>
		
