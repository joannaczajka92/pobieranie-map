<?php
set_time_limit(0);
ini_set("memory_limit","3800M");
		
$longitude=(float)$_GET['lng'];
$latitude=(float)$_GET['lat'];

$x = ($longitude + 180) / 360; 
$sinLatitude = sin($latitude * pi() / 180);
$y = 0.5 - log((1 + $sinLatitude) / (1 - $sinLatitude)) / (4 * pi());
$zoomMap=(int)$_GET['zoomMap'];
$zoom=(int)$_GET['amountInput'];
$mapSize = MapSize($zoom);
$pixelX = Clip($x * $mapSize + 0.5, 0, $mapSize - 1);
$pixelY = Clip($y * $mapSize + 0.5, 0, $mapSize - 1);

function Clip($n, $minValue, $maxValue)
{
	return min(max($n, $minValue), $maxValue);
}

function MapSize($zoomLevel)
{
	return (int) 256 << $zoomLevel;
}

		
function TileXYToQuadKey($tileX, $tileY, $zoomLevel)
{
	$quadKey = "";
	for ($i = $zoomLevel; $i > 0; $i--)
	{
		$digit = '0';
		$mask = 1 << ($i - 1);
		if (($tileX & $mask) != 0)
		{
			$digit++;
		}
		if (($tileY & $mask) != 0)
		{
			$digit++;
			$digit++;
		}
		
		$quadKey .= $digit;
	}
	return $quadKey;
}

		
$tileX=floor(($pixelX/256));
$tileY=floor(($pixelY/256));
	
$widthMap=pow(2, ($zoom-$zoomMap))*2;
$heightMap=pow(2, ($zoom-$zoomMap))*2;
		
$mapa=imagecreatetruecolor($widthMap*256, $heightMap*256);
		
if($_GET['mapaBingType'] == 'basicBing') {

for($x=0; $x<$widthMap; $x++) {

		for($y=0; $y<$heightMap; $y++) {
				
			$tileX=floor(($pixelX/256)+$x);
			$tileY=floor(($pixelY/256)+$y);
			
			$tileX-=$widthMap/2;
			$tileY-=$heightMap/2;
								
			$url="http://ecn.t1.tiles.virtualearth.net/tiles/r".((string)(TileXYToQuadKey($tileX, $tileY, $zoom)))."?g=796&mkt=en-us&lbl=l1&stl=h&shading=hill&n=z";
			
			if($zoom < 12){
				
				$src = imagecreatefromjpeg($url);
				
			}else{
			
				$src = imagecreatefrompng($url);
			}
			
			imagecopymerge($mapa, $src, ($x*256), ($y*256), 0, 0, 256, 256, 100);
			imagedestroy($src);
	
			}
	}
	
} else if ($_GET['mapaBingType'] == 'satellite') {

	for($x=0; $x<$widthMap; $x++) {
	
		for($y=0; $y<$heightMap; $y++) {
				
			$tileX=floor(($pixelX/256)+$x);
			$tileY=floor(($pixelY/256)+$y);
	
			$tileX-=$widthMap/2;
			$tileY-=$heightMap/2;
	
			$url="http://ecn.t1.tiles.virtualearth.net/tiles/a".((string)(TileXYToQuadKey($tileX, $tileY, $zoom))).".jpeg?g=1062&n=z";
			$src = imagecreatefromjpeg($url);
			imagecopymerge($mapa, $src, ($x*256), ($y*256), 0, 0, 256, 256, 100);
			imagedestroy($src);
	
			}
	}

} else if ($_GET['mapaBingType'] == 'satelliteLabel') {

	for($x=0; $x<$widthMap; $x++) {
	
		for($y=0; $y<$heightMap; $y++) {
	
			$tileX=floor(($pixelX/256)+$x);
			$tileY=floor(($pixelY/256)+$y);
	
			$tileX-=$widthMap/2;
			$tileY-=$heightMap/2;
						
			$url="http://ecn.dynamic.t1.tiles.virtualearth.net/comp/CompositionHandler/".((string)(TileXYToQuadKey($tileX, $tileY, $zoom)))."?mkt=en-us&it=A,G,L&n=z";
			$src = imagecreatefromjpeg($url);
			imagecopymerge($mapa, $src, ($x*256), ($y*256), 0, 0, 256, 256, 100);
			imagedestroy($src);
	
			}
		}
}

if($_GET['format'] == 'png') {

	imagepng($mapa, ''.$_GET['nameFile'].'.'.$_GET['format'].'');
	
}else {
	
	imagejpeg($mapa, ''.$_GET['nameFile'].'.'.$_GET['format'].'');

}

imagedestroy($mapa);
header('Content-Type: image/png');

?>
		
		
