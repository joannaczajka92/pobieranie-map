<?php
	
set_time_limit(0);
ini_set("memory_limit","3800M");
		
$lon=(float)$_GET['lng'];
$lat=(float)$_GET['lat'];


$zoom=$_GET['amountInput'];
$xs = floor((($lon + 180) / 360) * pow(2, $zoom));
$ys = floor((1 - log(tan(deg2rad($lat)) + 1 / cos(deg2rad($lat))) / pi()) /2 * pow(2, $zoom));

$zoomMap=(int)$_GET['zoomMap'];

$widthMap=pow(2, ($zoom-$zoomMap))*2;
$heightMap=pow(2, ($zoom-$zoomMap))*2;

$mapa=imagecreatetruecolor($widthMap*256, $heightMap*256);

$xs-=$widthMap/2;
$ys-=$heightMap/2;


if($_GET['mapaTypeOSM'] == 'basic'){

	$url_s="http://tile.openstreetmap.org/";

	for($x=0; $x<=$widthMap; $x++) {
	
		for($y=0; $y<=$heightMap; $y++) {
		
			$url=$url_s.(string)($zoom)."/".((string)($xs+$x))."/".((string)($ys+$y)).".png";
			$src = imagecreatefrompng($url);		
			imagecopymerge($mapa, $src, ($x*256), ($y*256), 0, 0, 256, 256, 100);
			imagedestroy($src);
			
		}
	
	}
} else if ($_GET['mapaTypeOSM'] == 'cycleMap') {

	$url_s="http://tile.opencyclemap.org/cycle/";

	for($x=0; $x<=$widthMap; $x++) {
	
		for($y=0; $y<=$heightMap; $y++) {
		
		$url=$url_s.(string)($zoom)."/".((string)($xs+$x))."/".((string)($ys+$y)).".png";
		$src = imagecreatefrompng($url);		
		imagecopymerge($mapa, $src, ($x*256), ($y*256), 0, 0, 256, 256, 100);
		imagedestroy($src);
		}
	}
				
}

if($_GET['format'] == 'png') {

	imagepng($mapa, ''.$_GET['nameFile'].'.'.$_GET['format'].'');

}else {
	
	imagejpeg($mapa, ''.$_GET['nameFile'].'.'.$_GET['format'].'');
}

imagedestroy($mapa);

header('Content-Type: image/png');	

?>
		
